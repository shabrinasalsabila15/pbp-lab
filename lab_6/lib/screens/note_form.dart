import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const appTitle = 'Take A Note App';

    return MaterialApp(
      title: appTitle,
      home: Scaffold(
        body: MyCustomForm(),
      ),
    );
  }
}

// Create a Form widget.
class MyCustomForm extends StatefulWidget {
  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

class MyCustomFormState extends State<MyCustomForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Joeurnnal 좋은날"
        ),
        backgroundColor: Colors.grey[900],
      ),
      body: Padding(
        padding: EdgeInsets.all(25.0),
        child: Center(
          child: Form(
            
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Take A Note',
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                ),
                Padding(
                  padding: EdgeInsets.only(
                    top: 20.0,
                  ),
                ),
                TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(
                        color : Color.fromRGBO(141, 129, 204, 1),
                      ),
                    ),

                    labelText: "Title"
                  ),
                ),

                Padding(
                  padding: EdgeInsets.only(
                    top: 20.0,
                  ),
                ),
                
                TextFormField(
                  
                  maxLines:7,
                  decoration: InputDecoration(
                    labelText: "Notes",
                    border: OutlineInputBorder(
                      
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(
                        color : Color.fromRGBO(141, 129, 204, 1),
                      ),
                    ),

                  ),
                ),
              Padding(

                padding: EdgeInsets.only(
                  top: 20.0,
                ),
              
                child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[ 
                                    RaisedButton(
                                      shape:  RoundedRectangleBorder(
                                                borderRadius: BorderRadius.circular(10.0)
                                              ),
                                      color : Color.fromRGBO(141, 129, 204, 1),
                                      onPressed: (){},
                                      child: Text("Save", style: TextStyle(color: Colors.white),),
                                    ),

                                    Padding(
                                      padding: EdgeInsets.only(
                                                left: 40.0,
                                                ),
                                    ),
                                    RaisedButton(
                                      shape:  RoundedRectangleBorder(
                                                borderRadius: BorderRadius.circular(10.0)
                                              ),
                                      color : Color.fromRGBO(141, 129, 204, 1),
                                      onPressed: (){},
                                      child: Text("Cancel",style: TextStyle(color: Colors.white)),
                                    )
                        ],
                      ),
              ),

            ],
          ),
        ),
      ),
          
      ),
        
    );
  }
}