from django.shortcuts import render
from lab_2.models import Note
from django.http.response import HttpResponseRedirect
from .forms import NoteForm
from django.contrib.auth.decorators import login_required

# Create your views here.

@login_required(login_url='/admin/login/')
def index(request):
    note = Note.objects.all()
    response = {'note': note}
    return render(request, 'lab4_index.html', response)
    
@login_required(login_url='/admin/login/')
def add_note(request):
    response = {}
    note = NoteForm(request.POST or None)
    if(note.is_valid() and request.method == 'POST'):
        note.save()
        return HttpResponseRedirect('/lab-4')
    response['note'] = note
    return render(request, 'lab4_form.html', response)

@login_required(login_url='/admin/login/')
def note_list(request):
    note = Note.objects.all()
    response = {'note': note}
    return render(request, 'lab4_note_list.html', response)