from django.http import response
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from .models import Friend
from .forms import FriendForm
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all()  # TODO Implement this
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    response = {}
    dari = FriendForm(request.POST or None)
    if(dari.is_valid() and request.method == 'POST'):
        dari.save()
        return HttpResponseRedirect('/lab-3')
    response['dari'] = dari
    return render(request, 'lab3_form.html', response)