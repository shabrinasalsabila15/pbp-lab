from lab_2.models import Note
from django.contrib import admin

# Register your models here.
from .models import Note

admin.site.register(Note)