import 'package:flutter/material.dart';
import 'package:lab_7/screen/note_form.dart';

class CardNote extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Joeurnnal 좋은날',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Joeurnnal 좋은날'),
          backgroundColor: Colors.grey[900],
        ),
        body: Container(
          padding: EdgeInsets.all(10.0),
          child: ListView(
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 20.0),
                  RaisedButton(
                    onPressed: () {
                      Navigator.push(context,
                      MaterialPageRoute(builder: (context) => NoteForm()));
                    },
                    elevation: 10.0,
                    color: Color.fromRGBO(141, 129, 204, 1),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0))),
                    child: Text(
                      '+ New Note',
                      style: TextStyle(color: Colors.white, fontSize: 15.0),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.only(
                  top: 10.0,
                ),
              ),

              Card(
                elevation: 10.0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)),
                color: Color.fromRGBO(141, 129, 204, 1),
                child: Container(
                  height: 150,
                  padding: EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ButtonBar(
                        children: [
                          RaisedButton(
                            child: Text(
                              'Delete',
                              style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'Poppins',
                              ),
                            ),
                            color: Colors.white,
                            onPressed: () {},
                          ),
                        ],
                      ),
                      Text(
                        'Judul Note',
                        style: TextStyle(
                            fontSize: 25.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                            fontFamily: 'Poppins'),
                      ),
                      SizedBox(height: 10.0),
                      Text(
                        'Isi catatan, ditulis di dalam kotak ini',
                        style: TextStyle(
                          fontSize: 20.0,
                          color: Colors.white,
                          fontFamily: 'Poppins',
                        ),
                      ),
                    ],
                  ),
                ),
              ), //card

              Padding(
                padding: EdgeInsets.only(
                  top: 10.0,
                ),
              ),

              Card(
                elevation: 10.0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)),
                color: Color.fromRGBO(141, 129, 204, 1),
                child: Container(
                  height: 150,
                  padding: EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ButtonBar(
                        children: [
                          RaisedButton(
                            child: Text(
                              'Delete',
                              style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'Poppins',
                              ),
                            ),
                            color: Colors.white,
                            onPressed: () {},
                          ),
                        ],
                      ),
                      Text(
                        'Judul Note',
                        style: TextStyle(
                            fontSize: 25.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                            fontFamily: 'Poppins'),
                      ),
                      SizedBox(height: 10.0),
                      Text(
                        'Isi catatan, ditulis di dalam kotak ini',
                        style: TextStyle(
                          fontSize: 20.0,
                          color: Colors.white,
                          fontFamily: 'Poppins',
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
