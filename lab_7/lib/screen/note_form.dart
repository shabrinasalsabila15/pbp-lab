import 'package:flutter/material.dart';
import 'package:lab_7/screen/card_note.dart';

class NoteForm extends StatelessWidget {
  const NoteForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const appTitle = 'Take A Note App';

    return MaterialApp(
      title: appTitle,
      home: Scaffold(
        body: MyCustomForm(),
      ),
    );
  }
}

// Create a Form widget.
class MyCustomForm extends StatefulWidget {
  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

class MyCustomFormState extends State<MyCustomForm> {
  final _formKey = GlobalKey<FormState>();
  String judul = "";
  String isiNote = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
                title: Text("Joeurnnal 좋은날"),
                backgroundColor: Colors.grey[900],
              ),
      body: Padding(
              padding: EdgeInsets.all(25.0),
              child: Center(
                      child: Form(
                              key: _formKey,
                              child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        Text(
                                          'Take A Note',
                                          style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(
                                                    top: 20.0,
                                          ),
                                        ),
                                        TextFormField(
                                          decoration: InputDecoration(
                                              border: OutlineInputBorder(
                                                        borderRadius: BorderRadius.circular(10),
                                                        borderSide: BorderSide(
                                                          color: Color.fromRGBO(141, 129, 204, 1),
                                                        ),
                                              ),
                                              labelText: "Title"),
                                          validator: (value) {
                                            if (value == null || value.isEmpty) {
                                              return 'Please add your title note';
                                            }
                                            judul = value;
                                            return null;
                                          },
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(
                                                    top: 20.0,
                                          ),
                                        ),
                                        TextFormField(
                                          maxLines: 7,
                                          decoration: InputDecoration(
                                                        labelText: "Notes",
                                                        border: OutlineInputBorder(
                                                                  borderRadius: BorderRadius.circular(10),
                                                                  borderSide: BorderSide(
                                                                                color: Color.fromRGBO(141, 129, 204, 1),
                                                                              ),
                                                        ),
                                                      ),
                                          validator: (value) {
                                            if (value == null || value.isEmpty) {
                                              return 'Please add your note';
                                            }
                                            isiNote = value;
                                            return null;
                                          },
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(
                                                    top: 20.0,
                                          ),
                                          child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    RaisedButton(
                                                      shape:  RoundedRectangleBorder(
                                                                borderRadius: BorderRadius.circular(10.0)
                                                              ),
                                                      color: Color.fromRGBO(141, 129, 204, 1),
                                                      onPressed: () {
                                                        if (_formKey.currentState!.validate()) {
                                                          print("Title: " + judul);
                                                          print("Note:\n" + isiNote);

                                                          showDialog(
                                                              context: context,
                                                              builder: (context) {
                                                                return AlertDialog(
                                                                        content: Text("Succesfully save your note!\n\nTitle: "+judul+"\nNote:\n"+isiNote),
                                                                        actions: <Widget>[
                                                                                    TextButton(
                                                                                      onPressed:(){
                                                                                        Navigator.of(context).push(MaterialPageRoute(
                                                                                        builder: (context) => NoteForm()));
                                                                                      },
                                                                                      child: Text('OK'),
                                                                                    ),
                                                                                  ],
                                                                      );
                                                          });
                                                        }
                                                      },

                                                      child: Text("Save", style: TextStyle(color: Colors.white),),
                                                    ),

                                                    Padding(
                                                      padding: EdgeInsets.only(
                                                                left: 40.0,
                                                      ),
                                                    ),

                                                    RaisedButton(
                                                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
                                                      color: Color.fromRGBO(141, 129, 204, 1),
                                                      onPressed: () {
                                                        Navigator.of(context).push(MaterialPageRoute(
                                                        builder: (context) => CardNote()));
                                                      },
                                                      child: Text("Cancel", style: TextStyle(color: Colors.white)),
                                                    )
                                                  ],
                                                ),
                                        ),
                                      ],
                                    ),
                            ),
                    ),
            ),
    );
  }

}
