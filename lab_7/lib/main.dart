import 'package:flutter/material.dart';
import 'package:lab_7/screen/card_note.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: CardNote(),
    );
  }
}