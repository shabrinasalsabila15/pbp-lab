import 'package:flutter/material.dart';

import '../widgets/main_drawer.dart';

// void main() => runApp(MyApp());

class CardNote extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // Application name
      title: 'Take A Note App',
      // Application theme data, you can set the colors for the application as
      // you want
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      // A widget which will be started on application startup
      home: MyHomePage(title: 'Take A Note App'),
    );
  }
}

class MyHomePage extends StatelessWidget {
  final String title;

  const MyHomePage({this.title});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // The title text which will be shown on the action bar
        title: Text(title),
      ),
        body: Card(
          elevation: 10.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0)
          ),
          color : Colors.purple,
          child: Container(
            height: 150,
            padding: EdgeInsets.all(10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ButtonBar(
                  children: [
                    RaisedButton(
                      child:  Text('Detele', 
                                  style:  TextStyle(
                                          color: Colors.black, 
                                          fontFamily: 'Poppins',
                                          ),
                              ),
                      color: Colors.white,
                      onPressed: (){},
                    ),                    
                  ],
                ),

                Text(
                  'Judul Note',
                  style: TextStyle(
                    fontSize: 25.0, 
                    fontWeight: FontWeight.bold, 
                    color: Colors.white,
                    fontFamily: 'Poppins'
                    ),
                ),
                SizedBox(height: 10.0),
                Text(
                  'Isi catatan, ditulis di dalam kotak ini',
                  style: TextStyle(
                    fontSize: 20.0, 
                    color: Colors.white, 
                    fontFamily: 'Poppins',
                    ),
                  
                ),
              ],
            ),
          ),
        ),
    );
  }
}